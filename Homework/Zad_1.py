# # Zad.1
# # Chciałbym aby po wykonaniu `print(person)` wyświetliły się dane z dowodu (imię, nazwisko, pesel, nr.dowodu).
# # Człowiek może zawierać tylko 1 zmienną.
#
# # Kasia Bartodziej
#
# class ID:
#     def __init__(self, name, surname, pesel, ID):
#         self.name = name
#         self.surname = surname
#         self.pesel = pesel
#         self.ID = ID
# class Person:
#     def __init__(self, id):
#             self.id = id
#     def __str__(self):
#         return ('{}, {}, {}, {}'.format(self.id.name, self.id.surname, self.id.pesel, self.id.ID))
#
# if __name__ == "__main__":
#     docID = ID('Stefan', 'Nowak','88120211148','ABC123456')
#     person = Person(docID)
#     print(person)

# --- poprawki KArol

class ID:
    def __init__(self, name, surname, pesel, ID):
        self.name = name
        self.surname = surname
        self.pesel = pesel
        self.ID = ID

    def __str__(self):
        return self.name + " " + self.surname + " " + self.pesel + " " + self.ID

class Person:
    def __init__(self, id):
            self.id = id
            self.degree = 'Bechelor'
    def __str__(self):
        return self.id.__str__() + " " + self.degree

if __name__ == "__main__":
    docID = ID('Stefan', 'Nowak','88120211148','ABC123456')
    person = Person(docID)
    print(docID)
    print(person)