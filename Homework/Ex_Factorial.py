def factorial(n, depth=1):
    if n == 1:
        print('\t' * depth, 'IReturning 1')
        return 1
    else:
        print('\t' * depth, 'Recursively calling factorial(', n - 1, ')')
        result = n * factorial(n - 1, depth + 1)
        print('\t' * depth, 'EReturning:', n, result)
        return result


print('Calling factorial( 5 )')
print(factorial(5))