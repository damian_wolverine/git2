# # ZADANIE 5
# #
# # newList = [ expression for item in list if conditional ]
# # Zamieńcie ten kod na zzwykłą pętlę for i wpakujcie w funkcję
# # listOfWords = ["this","is","a","list","of","words"]
# # items = [ word[0] for word in listOfWords ]
#
# listOfWords = ["this", "is", "a", "list", "of", "words"]
# items = [word[0] for word in listOfWords]
# print(items)
#
# ls = []
# for i in listOfWords:
#     ls.append(i[0])
#
# print(ls, "moja lista")

#ZADANIE 9
# Napisz funkcję która utworzy set na podstawie innego seta,
# jeśli wartość elementu będzie większa od 20 to pomnóż ją razy 5
# list comprehension

# def create_new_set(s):
#     return {i*5 if i > 20 else i for i in s}
#     # outset = {i*5 if i > 20 else i for i in s}
#     # return outset
#
#
# if __name__ == "__main__":
#     inset = set()
#     for i in range(0,31,5):
#         inset.add(i)
#     print(inset)
#     result = create_new_set(inset)
#     print(result)