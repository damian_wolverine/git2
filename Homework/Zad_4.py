
class Characters:
    hp = 50
    mp = 50

    def __init__(self):
        pass

    def attack(self):
        pass

    def takeDmg(self, dmgAmount):
        self.hp -= dmgAmount

class Warrior (Characters):
    def __init__(self):
        super(Warrior,self).__init__()

class Mage (Characters):
    def __init__(self):
        super(Mage,self).__init__()

    def attack(self):
            self.mp -= 10

character = Characters()
magic = Mage()
warrior = Warrior()
# Kod:
#
print(character.hp) # 50
print(character.mp) # 50
character.attack()
print(character.hp) # 50
print(character.mp) # 50

print(magic.hp) # 50
print(magic.mp) # 50
magic.attack()
print(magic.hp) # 50
print(magic.mp) # 40

print(warrior.hp) # 50
print(warrior.mp) # 50
warrior.attack()
print(warrior.hp) # 50
print(warrior.mp) # 50