# To teraz zamieńmy naszą klasę POjazd na klasę czysto abstrakcyjną
# pojazd czyli główna klasa z której bedzimy poberać włąściwości ale sama w sobie nie powinna tworzyć obiektu

from abc import ABC, abstractmethod

# czyli nasza najwyższa klasa musi dziedziczycć włąsnie z ABC czyli abstractmethod
class Vehicle(ABC):

    @abstractmethod
    def go(self):
        pass

class Car(Vehicle):
    def __init__(self, speedC):
        self.speedC = speedC

    def go(self):
        self.speedC += 2

    def TurnOnMotor(self):
        return True

class Bicycle(Vehicle):
    def __init__(self, speedB = 0):
        self.speedB = speedB

    def go(self):
        self.speedB += 1


if __name__ == "__main__":
    # v = Vehicle() # wyrzuca mi błąd bo nie będzie obiektu klas abstrakcyjna
    # v.go()
    # c = Car(10)
    # print(c.speedC)
    # c = Car(10)
    # print(c.go())
    b = Bicycle()
    b.go()
    b.go()
    print(b.speedB) # aby dowiedziec sie jaka jest predkosc trzeba najpierw wywołąć metodę go

# --- Rozwiazanie Karola
#

from abc import ABC, abstractmethod
class Vehicle(ABC):
    @abstractmethod
    def start(self):
        pass
    @abstractmethod
    def stop(self):
        pass
class Car(Vehicle):
    def __init__(self, model, brand):
        self.model = model
        self.brand = brand
        self.currentSpeed = 0
        self.isMotorOn = False
    def start(self):
        if self.currentSpeed == 0:
            self.isMotorOn = True
        self.currentSpeed += 2
    def stop(self):
        if self.currentSpeed == 0:
            self.isMotorOn = False
        self.currentSpeed -= 2
    def turnOffMotor(self):
        self.isMotorOn = False
class Bicycle(Vehicle):
    def __init__(self, model, brand):
        self.model = model
        self.brand = brand
        self.currentSpeed = 0
    def start(self):
        self.currentSpeed += 1
    def stop(self):
        self.currentSpeed -= 1
car = Car("e36", "BMW")
bicycle = Bicycle("damka", "markaRower")
bicycle.start()
car.start()
print("Car start")
print(car.currentSpeed)
print("Bicycle start")
print(bicycle.currentSpeed)
car.__class__ = Vehicle
bicycle.__class__ = Vehicle # możemy odwołąć się do POjazdu czyli nasz rower jest pojazdem
print("Car as Vehicle start")
car.start()
print("Bicycle as Vehicle start")
bicycle.start()
print(car.currentSpeed)
print(bicycle.currentSpeed)
print("Bicycle as Car start")
bicycle.__class__ = Car
#print(bicycle.isMotorOn)
bicycle.turnOffMotor() # utworzenie zmiennej w locie
print(bicycle.isMotorOn)